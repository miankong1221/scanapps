import { Injectable } from "@angular/core";

@Injectable()
export class Wb {

    xmlHttpRequest: XMLHttpRequest;

    result: any;

    constructor() {
    }

    webService(type: string, url: string, async: boolean,
        contentType: string, soapAction: string, body: string) {
        this.getXmlHttp();
        this.xmlHttpRequest.open(type, url, async);
        this.xmlHttpRequest.onreadystatechange = () => {
            this.result = this.xmlHttpRequest.responseXML;
            console.log(this.result);
        }
        this.xmlHttpRequest.setRequestHeader('Content-Type', contentType);
        this.xmlHttpRequest.setRequestHeader('SOAPAction', soapAction);
        this.xmlHttpRequest.send(body);
        return this.result
    }

    getXmlHttp(){
        this.xmlHttpRequest = new XMLHttpRequest();
    }
}