import { Injectable } from "@angular/core";
import { LpEntity } from "../../pages/lp-search/lpEntity";
import { MyJobEntity } from "../../pages/continue-my-job/continue-my-job-entity";

@Injectable()
export class LpSearchState{

    public soNumber: string;

    public poNumber: string;

    public sku: string;

    public lpinfoList: LpEntity[];

    public paramJobList: MyJobEntity[];

    constructor(){
        this.soNumber = undefined;
        this.poNumber = undefined;
        this.sku = undefined;
        this.lpinfoList = [];
        this.paramJobList = [];
    }

}