import { SQLite, SQLiteObject } from "@ionic-native/sqlite";
import { Events, AlertController } from "ionic-angular";

export class DataProvider {

  myAppDatabase: SQLiteObject;

  constructor(
    private sqlite: SQLite,
    private events: Events
  ) {

  }

  createDb() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        this.myAppDatabase = db
        this.events.publish('db:create');
      })
      .catch(e => {
        this.events.publish('error')
      });
  }

  executeSql(sql: string, array: Array<string>): Promise<any> {
    return new Promise((resolve, reject) => {
          this.myAppDatabase.executeSql(sql, array).then((data) => {
            resolve(data);
          }, (err) => {
            reject(err);
            console.log('Unable to execute sql: ' + err);
          });
    });
  }

}