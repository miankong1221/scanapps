export class LpEntity {

    client: string;

    lpNumber: string;

    poNumber: string;

    sku: string;

    soNumber: string;

    cartonId: string;
    
    cartonStatus: string;

    constructor(){
        this.client = undefined;
        this.lpNumber = undefined;
        this.poNumber = undefined;
        this.sku = undefined;
        this.soNumber = undefined;
        this.cartonId = undefined
        this.cartonStatus = undefined;
    }

}