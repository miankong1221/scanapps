import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LpSearchPage } from './lp-search';


@NgModule({
  declarations: [
    LpSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(LpSearchPage),
  ]
})
export class LpSearchPageModule {}
