import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform, AlertController } from 'ionic-angular';
import { HomePage, PreScanPage, ProfileEntity } from '../';
import { LpSearchState, DataProvider } from '../../providers';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { LpEntity } from './lpEntity';


/**
 * Generated class for the LpSearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lp-search',
  templateUrl: 'lp-search.html',
})
export class LpSearchPage {



  private lpinfoList: LpEntity[];
  private client: string;
  private profileInfoList: ProfileEntity[];
  private poPattern: RegExp;
  private soPattern: RegExp;
  private skuPattern: RegExp;
  private poMaxLength: number;
  private soMaxLength: number;
  private skuMaxLength: number;
  private myGroup: FormGroup;
  private currentLpIsExist: boolean;
  private currentLpNumber: any;

  formErrors = {
    poNumber: '',
    soNumber: '',
    sku: ''
  }

  validationMessage = {
    'poNumber': {
      'maxlength': 'Please input no more than 10 character',
      'required': 'Please input po',
      'pattern': 'po format is not correct'
    },
    'soNumber': {
      'maxlength': 'Please input no more than 10 character',
      'required': 'Please input so',
      'pattern': 'so format is not correct'
    },
    'sku': {
      'maxlength': 'Please input no more than 10 character',
      'required': 'Please input sku',
      'pattern': 'sku format is not correct'
    }
  }


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public lpSearchState: LpSearchState,
    public loadingCtrl: LoadingController,
    private dataProvider: DataProvider,
    private fb: FormBuilder) {

    let temp = this.navParams.get('forwardParam');
    let requestParam = JSON.parse(temp);
    this.client = requestParam.profileEntityList[0].client;
    this.lpinfoList = [];
    this.profileInfoList = [];
    this.currentLpIsExist = false;
    // mock data
    const adidaslpOne = new LpEntity();
    adidaslpOne.lpNumber = '123-ADIDAS-20200820'
    adidaslpOne.poNumber = 'ADIDASEFGH';
    adidaslpOne.soNumber = 'YAT1234567';
    adidaslpOne.sku = 'FREEST-YLE'
    adidaslpOne.client = 'ADIDAS';
    adidaslpOne.cartonId = 'ADIDAS111';
    adidaslpOne.cartonStatus = 'N';
    this.lpinfoList.push(adidaslpOne);
    const adidaslpTwo = new LpEntity();
    adidaslpTwo.lpNumber = '123-ADIDAS-20200820'
    adidaslpTwo.poNumber = 'ADIDASEFGH';
    adidaslpTwo.soNumber = 'YAT1234567';
    adidaslpTwo.sku = 'FREEST-YLE'
    adidaslpTwo.client = 'ADIDAS';
    adidaslpTwo.cartonId = 'ADIDAS222';
    adidaslpTwo.cartonStatus = 'N';
    this.lpinfoList.push(adidaslpTwo);
    const nikelpOne = new LpEntity();
    nikelpOne.lpNumber = '888-NIKE-20200820'
    nikelpOne.poNumber = 'NIKEASEFGH';
    nikelpOne.soNumber = 'YAT8888888';
    nikelpOne.sku = 'AIRJOR-DEN'
    nikelpOne.client = 'NIKE';
    nikelpOne.cartonId = 'NIKE333';
    nikelpOne.cartonStatus = 'N';
    this.lpinfoList.push(nikelpOne);
    const nikelpTwo = new LpEntity();
    nikelpTwo.lpNumber = '888-NIKE-20200820'
    nikelpTwo.poNumber = 'NIKEASEFGH';
    nikelpTwo.soNumber = 'YAT8888888';
    nikelpTwo.sku = 'AIRJOR-DEN'
    nikelpTwo.client = 'NIKE';
    nikelpTwo.cartonId = 'NIKE444';
    nikelpTwo.cartonStatus = 'N';
    this.lpinfoList.push(nikelpTwo);
    this.buildForm(requestParam);
  }
  buildForm(requestParam: any) {

    for (let i = 0; i < requestParam.profileEntityList.length; i++) {
      let temp = requestParam.profileEntityList[i];
      if (temp.displayName === 'PO') {
        let poFormat = temp.format;
        let poTemp = poFormat.replace(/A/g, '[A-Z]').replace(/9/g, '[1-9]')
        this.poPattern = new RegExp(poTemp);
        this.poMaxLength = Number(temp.maxLength);
      } else if (temp.displayName === 'SO') {
        let soFormat = temp.format;
        let soTemp = soFormat.replace(/A/g, '[A-Z]').replace(/9/g, '[1-9]')
        this.soPattern = new RegExp(soTemp);
        this.soMaxLength = Number(temp.maxLength);
      } else if (temp.displayName === 'SKU') {
        let skuFormat = temp.format;
        let skuTemp = skuFormat.replace(/A/g, '[A-Z]').replace(/9/g, '[1-9]');
        this.skuPattern = new RegExp(skuTemp);
        this.skuMaxLength = Number(temp.maxLength);
      }
    }
    alert(this.poPattern + '---' + this.soPattern + '---' + this.skuPattern);
    this.myGroup = this.fb.group({
      poNumber: [null, [Validators.required, Validators.maxLength(10), Validators.pattern(this.poPattern)]],
      soNumber: [null, [Validators.required, Validators.maxLength(10), Validators.pattern(this.soPattern)]],
      sku: [null, [Validators.required, Validators.maxLength(10), Validators.pattern(this.skuPattern)]]
    })
    this.myGroup.valueChanges.subscribe((data) => {
      this.onValueChanged(data);
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LpSearchPage');
  }

  onValueChanged(data?: any) {
    if (!this.myGroup) {
      return
    }
    const form = this.myGroup;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessage[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + '\n';
        }
      }
    }
    console.log(this.formErrors);
  }


  backHome() {
    this.navCtrl.push(HomePage);
  }

  async doSubmit() {
    this.lpSearchState.lpinfoList = [];
    this.lpSearchState.soNumber = this.myGroup.get('soNumber').value;
    this.lpSearchState.poNumber = this.myGroup.get('poNumber').value;
    this.lpSearchState.sku = this.myGroup.get('sku').value;
    if (this.formErrors.poNumber === '' &&
      this.formErrors.soNumber === '' &&
      this.formErrors.sku === '') {
      for (let i = 0; i < this.lpinfoList.length; i++) {
        let temp = this.lpinfoList[i];
        if (temp.client === this.client) {
          this.currentLpNumber = temp.lpNumber;
          this.lpSearchState.lpinfoList.push(temp);
        }
      }
      alert('lp number is ' + this.lpSearchState.lpinfoList[0].lpNumber);
      if (this.lpSearchState.soNumber !== null &&
        this.lpSearchState.poNumber !== null &&
        this.lpSearchState.sku !== null) {
        this.show();
      } else {
        alert('Please input all the field')
      }
    } else {
      alert('Please input correct format');
    }
    // TODO invoke webservice download LP
    // update database
  }

  show() {
    let loading = this.loadingCtrl.create({
      content: "Synchronize the LP",
      spinner: "crescent",
    })
    loading.present()
    this.updateLpTbl().then(() => {
      loading.dismiss().then(() => {
        this.navCtrl.push(PreScanPage, { client: this.client });
      })
    })
  }

  // update tbl
  async updateLpTbl() {
    // TODO invoke webservice downloadLp

    // create LP if not exist
    await this.createLpTbl();

    await this.selectLpTblByLpNum(this.currentLpNumber);
    // insert into with mock data
    if (this.currentLpIsExist === false) {
      this.lpinfoList.forEach(async (temp: LpEntity) => {
        if(this.lpinfoList.length > 0){
          if (this.client === temp.client) {
            await this.insertLpTbl(temp);
          }
        }else{
          alert('LP is not available')
        }
      })
    }
    // this.lpinfoList.forEach(async (temp: LpEntity) => {
    //   if(this.client === temp.client && this.currentLpIsExist === false){
    //     await this.insertLpTbl(temp);
    //   }
    // })


  }
  // create LP tbl
  createLpTbl() {
    let sql = 'CREATE TABLE IF NOT EXISTS lp (client VARCHAR(50), lpNumber VARCHAR(25), poNumber VARCHAR(25), sku VARCHAR(50), soNumber VARCHAR(20), cartonId VARCHAR(50), cartonStatus VARCHAR(10));'
    return this.dataProvider.executeSql(sql, [])
      .then(() => {
        console.log('create tbl successfully')
      });
  }

  // insert lp data
  insertLpTbl(lp: LpEntity) {
    return this.dataProvider.executeSql('INSERT INTO lp VALUES (?, ?, ?, ?, ?, ?, ?)', [lp.client, lp.lpNumber, lp.poNumber, lp.sku, lp.soNumber, lp.cartonId, lp.cartonStatus])
      .then((data) => {
        console.log('insert tbl successfully');
      });
  }

  selectLpTblByLpNum(lpNumber: any) {
    let sql = 'SELECT * FROM lp where lpNumber = ?;';
    return this.dataProvider.executeSql(sql, [lpNumber])
      .then((data) => {
        if (data.rows.length > 0) {
          this.currentLpIsExist = true;
        }
      });

  }
}
