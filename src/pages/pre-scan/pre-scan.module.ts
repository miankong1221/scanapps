import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PreScanPage } from './pre-scan';


@NgModule({
  declarations: [
    PreScanPage,
  ],
  imports: [
    IonicPageModule.forChild(PreScanPage),
  ],
  exports: [
    PreScanPage
  ]
})
export class PreScanPageModule {}
