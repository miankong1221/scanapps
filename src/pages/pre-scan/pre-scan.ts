import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage, ScanPage } from '../';
import { LpSearchState, DataProvider } from '../../providers';
import { LpEntity } from '../lp-search/lpEntity';


/**
 * Generated class for the ScanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pre-scan',
  templateUrl: 'pre-scan.html',
})
export class PreScanPage {

  lpList: LpEntity[];

  client: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public lpSearchState: LpSearchState,
    private dataProvider: DataProvider) {
      this.lpList = []
      this.client = this.navParams.get('client');
  }

  ionViewDidLoad() {
    console.log(this.lpSearchState.sku);
    console.log('ionViewDidLoad ScanPage');
    this.selectLpTbl();
  }

  backHome() {
    this.navCtrl.push(HomePage);
  }

  scanCatonId() {
    this.navCtrl.push(ScanPage, {client: this.client});    
  }

  selectLpTbl() {
    let sql = 'SELECT * FROM lp;';
    this.dataProvider.executeSql(sql, [])
      .then((data) => {
        for (let i = 0; i < data.rows.length; i++) {
          const temp = new LpEntity()
          temp.lpNumber = data.rows.item(i).lpNumber;
          temp.client = data.rows.item(i).client;
          temp.poNumber = data.rows.item(i).poNumber;
          temp.soNumber = data.rows.item(i).soNumber;
          temp.sku = data.rows.item(i).sku;
          temp.cartonStatus = data.rows.item(i).cartonStatus;
          temp.cartonId = data.rows.item(i).cartonId;
          this.lpList.push(temp);
        }
      });
  }
}
