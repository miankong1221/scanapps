import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LpSearchState, DataProvider } from '../../providers';
import { LpSearchPage, ScanPage } from '..';
import { LpEntity } from '../lp-search/lpEntity';
import { MyJobEntity } from './continue-my-job-entity';

/**
 * Generated class for the ContinueMyJobPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-continue-my-job',
  templateUrl: 'continue-my-job.html',
})
export class ContinueMyJobPage {

  // public jobList: MyJobEntity[];

  public client: any;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public lpSearchState: LpSearchState,
    private dataProvider: DataProvider) {
    // this.jobList = [];
  }

  ionViewDidLoad() {

    // this.jobList = this.lpSearchState.paramJobList;
    console.log('ionViewDidLoad ContinueMyJobPage');
  }

  continue(job: MyJobEntity) {

    // this.selectLpTblByLpNum(job.lpName).then(() => {
    //   this.navCtrl.push(ScanPage,{client: this.lpSearchState.lpinfoList[0].client});
    //   this.lpSearchState.paramJobList = [];
    // });

    if(this.lpSearchState.lpinfoList.length > 0){
      for(let i = 0; i < this.lpSearchState.lpinfoList.length; i++){
        if(this.lpSearchState.lpinfoList[i].lpNumber = job.lpName){
          this.client = this.lpSearchState.lpinfoList[i].client;
          i = i + 100;
        }
      }
      this.navCtrl.push(ScanPage,{client: this.client});
    }else{
      this.selectLpTblByLpNum(job.lpName).then(() => {
        this.navCtrl.push(ScanPage,{client: this.lpSearchState.lpinfoList[0].client});
      });
    }
  }

  selectLpTblByLpNum(lpNumber: any) {
    let sql = 'SELECT * FROM lp where lpNumber = ?;';
    return this.dataProvider.executeSql(sql, [lpNumber])
      .then((data) => {
        for(let j = 0; j < data.rows.length; j++){
          let temp = new LpEntity();
          temp.cartonId = data.rows.item(j).cartonId;
          temp.cartonStatus = data.rows.item(j).cartonStatus;;
          temp.client = data.rows.item(j).client;
          temp.lpNumber = data.rows.item(j).lpNumber;
          temp.poNumber = data.rows.item(j).poNumber;
          temp.sku = data.rows.item(j).sku;
          temp.soNumber = data.rows.item(j).soNumber;
          this.lpSearchState.lpinfoList.push(temp);
        }
      })
  }
}
