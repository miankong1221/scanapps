import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContinueMyJobPage } from './continue-my-job';

@NgModule({
  declarations: [
    ContinueMyJobPage,
  ],
  imports: [
    IonicPageModule.forChild(ContinueMyJobPage),
  ],
})
export class ContinueMyJobPageModule {}
