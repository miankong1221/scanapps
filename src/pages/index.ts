// The page the user lands on after opening the app and without a session
export const FirstRunPage = 'TutorialPage';
// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
export const MainPage = 'TabsPage';
export const HomePage = 'HomePage';
export const ProfilePage = 'ProfilePage';
export const LpSearchPage = 'LpSearchPage';
export const PreScanPage = 'PreScanPage';
export const ScanPage = 'ScanPage';
export const SyncProfilePage = 'SyncProfilePage';
export const ContinueMyJobPage = 'ContinueMyJobPage';
// The initial root pages for our tabs (remove if not using tabs)
export const Tab1Root = 'ListMasterPage';
export const Tab2Root = 'SearchPage';
export const Tab3Root = 'SettingsPage';

export { ProfileEntity } from './profile/profileEntity';

