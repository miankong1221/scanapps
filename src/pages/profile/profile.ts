import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LpSearchPage, ProfileEntity } from '../';
import { DataProvider, LpSearchState } from '../../providers';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  client: string;

  profileList: ProfileEntity[];

  profileListView: any[];

  forwardParam: { profileEntityList: ProfileEntity[]};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public lpSearchState: LpSearchState,
    private dataProvider: DataProvider) {
    this.profileList = [];
    this.profileListView = [];
    this.forwardParam = { profileEntityList: [] };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
    // this.clientList[0] ='ADIDAS';
    // this.clientList[1] ='NIKE';
    // this.client = 'ADIDAS';

    this.selectProfileTbl()
    .then(()=>{
      let tempList = [];
      for(let i = 0; i < this.profileList.length; i++) {
        tempList[i] = this.profileList[i].client;
      }
      this.profileListView = Array.from(new Set(tempList));
    });
  }

  forward(client: string) {
    this.forwardParam = { profileEntityList: [] };
    for(let i = 0; i < this.profileList.length; i++){
      let temp = this.profileList[i];
      if(temp.client === client){
        this.forwardParam.profileEntityList.push(temp)
      }
    }
    this.navCtrl.push(LpSearchPage,{forwardParam: JSON.stringify(this.forwardParam)});
  }

  selectProfileTbl() {
    let sql = 'SELECT * FROM profile;';
    return this.dataProvider.executeSql(sql, [])
      .then((data) => {
        for (let j = 0; j < data.rows.length; j++) {
          const temp = new ProfileEntity();
          temp.client = data.rows.item(j).client;
          temp.code = data.rows.item(j).code;
          temp.profileName = data.rows.item(j).profileName;
          temp.type = data.rows.item(j).type;
          temp.displayName = data.rows.item(j).displayName;
          temp.format = data.rows.item(j).format;
          temp.maxLength = data.rows.item(j).maxLength;
          this.profileList.push(temp);
        }
      });
  }


}
