export class ProfileEntity {

    public code: string;

    public profileName: string;

    public client: string;

    public type: string;

    public displayName: string;

    public format: string;

    public maxLength: string;

    constructor() {
        this.code = undefined;
        this.profileName = undefined;
        this.client = undefined;
        this.type = undefined;
        this.displayName = undefined;
        this.format = undefined;
        this.maxLength = undefined;
    }
}
// export class DetailProfileEntity {

//     type: string;

//     displayName: string;

//     format: string;

//     maxLength: string;

//     constructor() {
//         this.type = undefined;
//         this.displayName = undefined;
//         this.format = undefined;
//         this.maxLength = undefined;
//     }
// }

// export class ProfileEntityView {

//     code: string;
//     profileName: string;
//     client: string;
//     type: string;
//     displayName: string;
//     format: string;
//     maxLength: string;

//     constructor(){
//         this.code = undefined;
//         this.profileName = undefined;
//         this.client = undefined;
//         this.type = undefined;
//         this.displayName = undefined;
//         this.format = undefined;
//         this.maxLength = undefined;
//     }
// }