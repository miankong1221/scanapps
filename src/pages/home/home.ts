import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform, AlertController, List } from 'ionic-angular';
import { ProfilePage, ProfileEntity, LpSearchPage, PreScanPage, ContinueMyJobPage } from '..';
import { DataProvider, Api, Wb, LpSearchState } from '../../providers';
import { LpEntity } from '../lp-search/lpEntity';
import { MyJobEntity } from '../continue-my-job/continue-my-job-entity';


/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  private wsResponse: any;

  private profileList: ProfileEntity[];

  private jobList: any[];

  private paramJobList: MyJobEntity[];

  // private sqlStatements: any[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public api: Api,
    public lpState: LpSearchState,
    private dataProvider: DataProvider,
    private wb: Wb
  ) {
    this.profileList = [];
    this.jobList = [];
    this.paramJobList = [];
    // this.sqlStatements = [];
    // mock data
    let profileAdPo = new ProfileEntity();
    profileAdPo.client = 'ADIDAS';
    profileAdPo.code = '00';
    profileAdPo.profileName = 'ADIDAS';
    profileAdPo.type = '11';
    profileAdPo.displayName = 'PO';
    profileAdPo.format = 'AAAAAAAAAA';
    profileAdPo.maxLength = '10';
    this.profileList.push(profileAdPo);
    let profileAdSo = new ProfileEntity();
    profileAdSo.client = 'ADIDAS';
    profileAdSo.code = '00';
    profileAdSo.profileName = 'ADIDAS';
    profileAdSo.type = '21';
    profileAdSo.displayName = 'SO';
    profileAdSo.format = 'AAA9999999';
    profileAdSo.maxLength = '10';
    this.profileList.push(profileAdSo);
    let profileAdSku = new ProfileEntity();
    profileAdSku.client = 'ADIDAS';
    profileAdSku.code = '00';
    profileAdSku.profileName = 'ADIDAS';
    profileAdSku.type = '31';
    profileAdSku.displayName = 'SKU';
    profileAdSku.format = 'AAAAAA-AAA';
    profileAdSku.maxLength = '10';
    this.profileList.push(profileAdSku);
    let profileNikePo = new ProfileEntity();
    profileNikePo.client = 'NIKE';
    profileNikePo.code = '00';
    profileNikePo.profileName = 'NIKE';
    profileNikePo.type = '11';
    profileNikePo.displayName = 'PO';
    profileNikePo.format = 'AAAAAAAAAA';
    profileNikePo.maxLength = '10';
    this.profileList.push(profileNikePo);
    let profileNikeSo = new ProfileEntity();
    profileNikeSo.client = 'NIKE';
    profileNikeSo.code = '00';
    profileNikeSo.profileName = 'NIKE';
    profileNikeSo.type = '21';
    profileNikeSo.displayName = 'SO';
    profileNikeSo.format = 'AAA9999999';
    profileNikeSo.maxLength = '10';
    this.profileList.push(profileNikeSo);
    let profileNikeSku = new ProfileEntity();
    profileNikeSku.client = 'NIKE';
    profileNikeSku.code = '00';
    profileNikeSku.profileName = 'NIKE';
    profileNikeSku.type = '31';
    profileNikeSku.displayName = 'SKU';
    profileNikeSku.format = 'AAAAAA-AAA';
    profileNikeSku.maxLength = '10';
    this.profileList.push(profileNikeSku);
    let profileTumiPo = new ProfileEntity();
    profileTumiPo.client = 'TUMI';
    profileTumiPo.code = '00';
    profileTumiPo.profileName = 'TUMI';
    profileTumiPo.type = '11';
    profileTumiPo.displayName = 'PO';
    profileTumiPo.format = 'AAAAAAA9A9';
    profileTumiPo.maxLength = '10';
    this.profileList.push(profileTumiPo);
    let profileTumiSo = new ProfileEntity();
    profileTumiSo.client = 'TUMI';
    profileTumiSo.code = '00';
    profileTumiSo.profileName = 'TUMI';
    profileTumiSo.type = '21';
    profileTumiSo.displayName = 'SO';
    profileTumiSo.format = 'AAAAA99999';
    profileTumiSo.maxLength = '10';
    this.profileList.push(profileTumiSo);
    let profileTumiSku = new ProfileEntity();
    profileTumiSku.client = 'TUMI';
    profileTumiSku.code = '00';
    profileTumiSku.profileName = 'TUMI';
    profileTumiSku.type = '31';
    profileTumiSku.displayName = 'SKU';
    profileTumiSku.format = '99AAAA-AAA';
    profileTumiSku.maxLength = '10';
    this.profileList.push(profileTumiSku);
  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad HomePage');
  }

  async continueMyJob() {
    
    this.lpState.paramJobList = [];
    await this.getJobList();
    // this.lpState.paramJobList = this.paramJobList;
    this.navCtrl.push(ContinueMyJobPage);
  }

  async syncProfile() {

    // Sync customer profile 
    this.wsResponse = this.getCustomerProfile();
    console.log(this.wsResponse);

    // TODO mapping entity

    // Loading during update TBL, then navigate to next page
    this.show();

    //this.navCtrl.push(LpSearchPage);


  }

  async updateTbl() {

    // Drop data from profile_tbl
    this.dropProfileTbl()

    // Create profile、lp tbl if not exist
    await this.createProfileTbl();
    await this.createLpTbl();

    // Insert data into profile_tbl


    this.profileList.forEach(async (temp: ProfileEntity) => {
      await this.insertProfileTbl(temp);
    })
    // await this.insertProfileTbl(this.profileList[0]);

  }

  getCustomerProfile() {
    let url = 'https://lnsscanservice.damco.com/LNSService.svc?wsdl';
    let contentType = 'text/xml';
    let soapAction = 'http://tempuri.org/ILNSService/GetProfile';
    let body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">'
      + '<soapenv:Header/>'
      + '<soapenv:Body>'
      + '<tem:GetProfile>'
      + '<tem:appUser>MXG004</tem:appUser>'
      + '<tem:appPwd>Damco123</tem:appPwd>'
      + '<tem:account>1</tem:account>'
      + '<tem:token>1</tem:token>'
      + '<tem:syncDatetime>2016-11-21T19:19:19</tem:syncDatetime>'
      + '<tem:syncDatetimeSpecified>2016-11-21T19:19:19</tem:syncDatetimeSpecified>'
      + '</tem:GetProfile>'
      + '</soapenv:Body>'
      + '</soapenv:Envelope>'
    //Todo parse result
    let result = this.wb.webService('POST', url, true, contentType, soapAction, body);
    console.log(result);
    return result
  }

  // create profile tbl
  createProfileTbl() {
    // let sql = 'CREATE TABLE IF NOT EXISTS users(email VARCHAR(320) PRIMARY KEY, username VARCHAR(20) NOT NULL, password VARCHAR(30) NOT NULL, gender BOOLEAN, age TINYINT, intro VARCHAR(300), phone CHAR(11), location VARCHAR(100));'
    let sql = 'CREATE TABLE IF NOT EXISTS profile(code VARCHAR(10), profileName VARCHAR(320), client VARCHAR(320), type VARCHAR(10), displayName VARCHAR(20), format VARCHAR(20), maxLength VARCHAR(10));'
    return this.dataProvider.executeSql(sql, [])
      .then(() => {
        console.log('create tbl successfully')
      });
  }

  // create LP tbl
  createLpTbl() {
    let sql = 'CREATE TABLE IF NOT EXISTS lp (client VARCHAR(50), lpNumber VARCHAR(25), poNumber VARCHAR(25), sku VARCHAR(50), soNumber VARCHAR(20), cartonId VARCHAR(50), cartonStatus VARCHAR(10));'
    return this.dataProvider.executeSql(sql, [])
      .then(() => {
        console.log('create tbl successfully')
      });
  }

  // insert porfile data
  insertProfileTbl(profile: ProfileEntity) {
    let temp = profile;
    return this.dataProvider.executeSql('INSERT INTO profile VALUES (?, ?, ?, ?, ?, ?, ?)', [temp.code, temp.profileName, temp.client, temp.type, temp.displayName, temp.format, temp.maxLength])
      .then((data) => {
        console.log('insert tbl successfully');
      });
  }

  dropProfileTbl() {
    let sql = 'DROP TABLE IF EXISTS profile'
    return this.dataProvider.executeSql(sql, [])
      .then(() => {
        console.log('drop tbl successfully');
      });
  }

  selectProfileTbl() {
    let sql = 'SELECT COUNT(*) FROM profile;';
    return this.dataProvider.executeSql(sql, [])
      .then(() => {
        console.log('select tbl successfully');
      });
  }

  async getJobList() {

    await this.createLpTbl();

    await this.selectLpTbl();
  
    if(this.jobList.length > 0){
      this.jobList.forEach(async (temp: any) => {
        await this.selectLpTblByLpNum(temp);
      })
    }

  

  }

  selectLpTbl() {
    let sql = 'SELECT distinct lpNumber FROM lp;';
    return this.dataProvider.executeSql(sql, [])
      .then((data) => {
        let i = 0;
        for (let j = 0; j < data.rows.length; j++) {
          let temp = data.rows.item(j).lpNumber;
          this.jobList[i] = temp;
          i++;
        }
      });
  }

  selectLpTblByLpNum(lpNumber: any){
    let sql = 'SELECT * FROM lp where lpNumber = ?;';
    return this.dataProvider.executeSql(sql, [lpNumber])
      .then((data) => {
        let totalNum = 0;
        let scannedNum = 0;
        for (let j = 0; j < data.rows.length; j++) {
          totalNum ++;
          if(data.rows.item(j).cartonStatus === 'Y'){
            scannedNum ++;
          }
        }
        let temp = new MyJobEntity();
        temp.scannedNum = scannedNum;
        temp.totalNum = totalNum;
        temp.lpName = lpNumber;
        this.lpState.paramJobList.push(temp);
      });

  }



  show() {
    let loading = this.loadingCtrl.create({
      content: "Synchronize the profile",
      spinner: "crescent",
    })
    loading.present()
    this.updateTbl().then(() => {
      loading.dismiss().then(() => {
        this.navCtrl.push(ProfilePage);
      })
    })
  }
}