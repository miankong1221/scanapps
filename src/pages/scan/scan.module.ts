import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScanPage } from './scan';
import { QRScanner } from '@ionic-native/qr-scanner';

@NgModule({
  declarations: [
    ScanPage,
  ],
  imports: [
    IonicPageModule.forChild(ScanPage),
  ],
  exports: [
    ScanPage,
  ],
  providers: [
    QRScanner,
  ]
})
export class ScanPageModule {}
