import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Alert } from 'ionic-angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { LpSearchState, DataProvider } from '../../providers';
import { HomePage } from '..';



/**
 * Generated class for the ScanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-scan',
  templateUrl: 'scan.html',
})
export class ScanPage {
  light: boolean;
  frontCamera: boolean;
  isShow: boolean = false;
  scannedNum: any;
  totalNum: any;
  viewScannedNum: any;
  sannedID: any;


  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private qrScanner: QRScanner,
    private viewCtrl: ViewController,
    public lpSearchState: LpSearchState,
    public _ngZone: NgZone,
    private dataProvider: DataProvider) {
      //默认为false
      this.light = false;
      this.frontCamera = false;
      // let client = this.navParams.get('client');
      // this.scannedNum = 0;
      // this.totalNum = 0;
      // for(let i = 0; i < this.lpSearchState.lpinfoList.length; i++){
      //   if(this.lpSearchState.lpinfoList[i].client === client){
      //     this.totalNum ++;
      //     if(this.lpSearchState.lpinfoList[i].cartonStatus === 'Y'){
      //       this.scannedNum ++;
      //     }
      //   }
      // }
      // this.viewScannedNum = 0;
  }

  ionViewDidLoad() {
    let client = this.navParams.get('client');
    this.scannedNum = 0;
    this.totalNum = 0;
    this.viewScannedNum = 0;
    for(let i = 0; i < this.lpSearchState.lpinfoList.length; i++){
      if(this.lpSearchState.lpinfoList[i].client === client){
        this.totalNum ++;
        if(this.lpSearchState.lpinfoList[i].cartonStatus === 'Y'){
          this.scannedNum ++;
        }
      }
    }
    this.viewScannedNum = String(this.scannedNum);
    this.invokeCamera();
    

  }

  ionViewDidEnter(){
    this.showCamera();
    this.isShow = true;
  }



  /**
   * 
   */
  toggleLight() {
    if (this.light) {
      this.qrScanner.disableLight();
    } else {
      this.qrScanner.enableLight();
    }
    this.light = !this.light;
  }

  /**
   * 
   */
  toggleCamera() {
    if (this.frontCamera) {
      this.qrScanner.useBackCamera();
    } else {
      this.qrScanner.useFrontCamera();
    }
    this.frontCamera = !this.frontCamera;
  }

  showCamera() {
    (window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');
  }

  hideCamera() {    
    (window.document.querySelector('ion-app') as HTMLElement).classList.remove('cameraView');
    this.qrScanner.hide();
    this.qrScanner.destroy();
  }

  ionViewWillLeave() {
    this.hideCamera();
  }

  invokeCamera(){
    return this.qrScanner.prepare()
    .then((status: QRScannerStatus) => {
      if (status.authorized) {
        // camera permission was granted
        // start scanning
        let scanSub = this.qrScanner.scan().subscribe((text: string) => { 
          let res = ()=>{
            for(let i = 0; i < this.lpSearchState.lpinfoList.length; i++){
              if(this.lpSearchState.lpinfoList[i].cartonId === text &&
                this.lpSearchState.lpinfoList[i].cartonStatus === 'N')
              {
                this.lpSearchState.lpinfoList[i].cartonStatus = 'Y';
                this.updateLpTbl(text);
                this._ngZone.run(() => {
                  this.sannedID = text;
                  this.scannedNum ++;
                  this.viewScannedNum = String(this.scannedNum);
                  // alert('Game Over')
                })
                if(this.scannedNum === this.totalNum){
                  alert('Game over');
                }
                // TODO update database
                return true
              }else if(this.lpSearchState.lpinfoList[i].cartonId === text &&
                this.lpSearchState.lpinfoList[i].cartonStatus === 'Y'){
                  alert('This carton has already benn sanned')
              }
            }
            alert('Carton ID is not correct');
            return false
          }
          res();
          // this.invokeCamera();
          this.qrScanner.hide(); // hide camera preview
          scanSub.unsubscribe(); // stop scanning
          this.invokeCamera();
          // if(this.scannedNum < this.totalNum){
          //   this.invokeCamera();
          // }else{
          //   this.hideCamera();
          // }
          // this.navCtrl.pop();
        });

        // show camera preview
        this.qrScanner.show();

        // wait for user to scan something, then the observable callback will be called
      } else if (status.denied) {
        // camera permission was permanently denied
        // you must use QRScanner.openSettings() method to guide the user to the settings page
        // then they can grant the permission from there
      } else {
        // permission was denied, but not permanently. You can ask for permission again at a later time.
      }
    })
    .catch((e: any) => console.log('Error is', e));
  }

  updateLpTbl(cartonId: string){
    return this.dataProvider.executeSql('UPDATE lp SET cartonStatus = ? WHERE cartonId = ?;', ["Y", cartonId])
      .then((data) => {
        alert('update tbl successfully')
        console.log('update tbl successfully');
      });
  }

  complete(){
    alert('upload successfully');
    this.navCtrl.push(HomePage);
  }
}
